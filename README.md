# react redux css-module starter kit
based on the ejected app from react-create-app

## stack
- yarn
- react:16.1.1
- redux:3.7.2
- react-redux:5.0.6
- react-hot-loader: 3.1.3

## dev
```
$ yarn start
```

## prd build test
```
$ export PUBLIC_URL=http://127.0.0.1
$ yarn build
$ docker run -v $PWD/build:/usr/share/nginx -p 80:80 nginx
## open your browser with http://127.0.0.1
```
