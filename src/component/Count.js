import React from 'react';
import { connect } from 'react-redux';
import { add } from '../action';

const mapStateToProps = (state,ownProps) => {
  return { count: state.count };
};
const mapDispatchToProps = (dispatch,ownProps) => {
  return {
    clickAddBtn: (n=1) => dispatch(add(n)),
  };
};

const Component = connect( 
  mapStateToProps,
  mapDispatchToProps
)( ({ count, clickAddBtn }) =>

  <div>
    <div>{count}</div>
    <button onClick={() => clickAddBtn(1)}>+1</button>
  </div>
);

export default Component;
