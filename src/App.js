import React from 'react';
import logo from './logo.svg';
import st from './App.css';
import Count from './component/Count';

export default () => (
  <div className={st.App}>
    <header className={st.header}>
      <img src={logo} className={st.logo} alt="logo" />
      <h1 className={st.title}>Welcome to React</h1>
    </header>
    <Count />
  </div>
);
