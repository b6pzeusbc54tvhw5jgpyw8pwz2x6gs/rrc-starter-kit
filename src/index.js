import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import './index.css';
import App from './App';
import rootReducer from './rootReducer';
import registerServiceWorker from './registerServiceWorker';
import { AppContainer } from 'react-hot-loader';

const store = createStore(rootReducer);

const render = Component => {
  ReactDOM.render(
    <AppContainer warnings={false}>
      <Provider store={store}>
        <Component />
      </Provider>
    </AppContainer>,
    document.getElementById('root'),
  )
};

render(App);

if( module.hot) {
  //const NextApp = require('./App').default;
  module.hot.accept('./App', () => render(App) )
  module.hot.accept('./rootReducer', () => store.replaceReducer(rootReducer) )
}

registerServiceWorker();
