import _ from 'underscore';

const type = t => `count:${_.uniqueId()}/${t}`;
const ADD = type('add');

export const reducer = (state=0,action) => {
  switch(action.type) {
    case ADD:
      state += action.size;
      break;

    default:
      break;
  }
  return state;
};

export const add = size => ({ type: ADD, size });

export default {
  reducer,
  add,
};
