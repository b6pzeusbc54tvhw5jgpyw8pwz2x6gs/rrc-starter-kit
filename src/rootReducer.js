import { combineReducers } from 'redux';
import { reducer as countReducer } from './module/countModule';

export default combineReducers({
  count: countReducer,
})

